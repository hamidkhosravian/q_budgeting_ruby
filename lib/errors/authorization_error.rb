# frozen_string_literal: true

class AuthorizationError < ::RuntimeError; end
