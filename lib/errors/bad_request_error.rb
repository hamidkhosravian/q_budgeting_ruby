# frozen_string_literal: true

class BadRequestError < ::RuntimeError; end
