# frozen_string_literal: true

class NotFoundError < ::RuntimeError; end
