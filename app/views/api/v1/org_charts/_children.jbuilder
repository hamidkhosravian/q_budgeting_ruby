# frozen_string_literal: true

json.children nodes do |node|
  json.partial! 'complete', org_chart: node
  json.partial! 'api/v1/org_charts/children', nodes: node.children if node.children.present?
end
