# frozen_string_literal: true

json.id org_chart.user.id
json.org_chart_id org_chart.id
json.personal_code org_chart.user.profile.personal_code
json.national_code org_chart.user.profile.national_code
json.first_name org_chart.user.profile.first_name
json.last_name org_chart.user.profile.last_name
json.position org_chart.position
json.avatar org_chart.user.profile.avatar&.abs_path
