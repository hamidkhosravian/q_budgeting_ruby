# frozen_string_literal: true

json.roots @roots do |root|
  json.partial! 'complete', org_chart: root
  json.partial! 'api/v1/org_charts/children', nodes: root.children
end
