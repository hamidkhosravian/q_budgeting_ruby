# frozen_string_literal: true

json.products @products do |product|
  json.partial! 'complete', product: product
  json.partial! 'api/v1/products/children', nodes: product.children
  json.budget_period do
    json.partial! 'api/v1/budget_periods/complete', budget_period: product.budget_period
  end
end
