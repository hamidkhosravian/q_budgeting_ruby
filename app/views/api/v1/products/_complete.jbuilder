# frozen_string_literal: true

json.id product.id
json.code product.code
json.name product.name
json.description product.description
json.product_type product.product_type
