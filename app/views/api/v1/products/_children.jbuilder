# frozen_string_literal: true

json.children nodes do |node|
  json.partial! 'complete', product: node
  json.partial! 'api/v1/products/children', nodes: node.children

  json.budget_period do
    json.partial! 'api/v1/budget_periods/complete', budget_period: node.budget_period
  end
end
