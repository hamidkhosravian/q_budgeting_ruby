# frozen_string_literal: true

json.product do
  json.partial! 'complete', product: @product
  json.parent do
    json.partial! 'complete', product: @product.parent if @product.parent.present?
  end
  json.budget_period do
    json.partial! 'api/v1/budget_periods/complete', budget_period: @product.budget_period
  end
end
