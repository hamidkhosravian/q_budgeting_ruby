# frozen_string_literal: true

json.id budget_period.id
json.code budget_period.code
json.name budget_period.name
json.start_date budget_period.start_date.to_date
json.end_date budget_period.end_date.to_date
json.description budget_period.description
