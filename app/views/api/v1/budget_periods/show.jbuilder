# frozen_string_literal: true

json.budget_period do
  json.partial! 'complete', budget_period: @budget_period
end
