# frozen_string_literal: true

json.budget_periods @budget_periods do |budget_period|
  json.partial! 'complete', budget_period: budget_period
end
