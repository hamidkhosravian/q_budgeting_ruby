# frozen_string_literal: true

def sum_capacity_per_month(product, month)
  product.select { |p| p.month == month }.map(&:total_capacity).sum
end

json.products @products do |product|
  json.partial! 'api/v1/products/complete', product: product
  json.budget_period do
    json.partial! 'api/v1/budget_periods/complete', budget_period: product.budget_period
  end
  json.total product.work_calendars.sum(:maximum_production_capacity)
  wk = product.work_calendars.group_by_month
  json.per_months @range_dates do |date|
    json.month date
    json.total sum_capacity_per_month(wk, date)
  end
end
