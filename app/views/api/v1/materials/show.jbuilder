# frozen_string_literal: true

json.material do
  json.partial! 'complete', material: @material
  json.parent do
    json.partial! 'complete', material: @material.parent if @material.parent.present?
  end
  json.budget_period do
    json.partial! 'api/v1/budget_periods/complete', budget_period: @material.budget_period
  end
end
