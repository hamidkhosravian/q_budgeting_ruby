# frozen_string_literal: true

json.call(material, :id, :code, :name, :description)
