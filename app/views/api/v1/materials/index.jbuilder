# frozen_string_literal: true

json.materials @materials do |material|
  json.partial! 'complete', material: material
  json.partial! 'api/v1/materials/children', nodes: material.children
  json.budget_period do
    json.partial! 'api/v1/budget_periods/complete', budget_period: material.budget_period
  end
end
