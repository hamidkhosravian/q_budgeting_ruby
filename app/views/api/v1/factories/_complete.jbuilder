# frozen_string_literal: true

json.id factory.id
json.code factory.code
json.name factory.name
json.city factory.city
json.state factory.state
json.postal_code factory.postal_code
json.phone_number factory.phone_number
json.address factory.address
json.description factory.description
