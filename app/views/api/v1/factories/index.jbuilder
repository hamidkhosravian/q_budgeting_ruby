# frozen_string_literal: true

json.factories @factories do |factory|
  json.partial! 'complete', factory: factory
end
