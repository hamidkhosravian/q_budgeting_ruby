# frozen_string_literal: true

json.factory do
  json.partial! 'complete', factory: @factory
end
