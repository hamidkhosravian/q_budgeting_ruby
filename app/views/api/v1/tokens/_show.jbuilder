# frozen_string_literal: true

json.id token.id
json.token token.token
