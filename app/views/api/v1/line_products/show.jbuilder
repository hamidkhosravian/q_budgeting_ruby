# frozen_string_literal: true

json.line_product do
  json.partial! 'complete', line_product: @line_product
  json.line do
    json.partial! 'api/v1/lines/complete', line: @line_product.line
    json.factory do
      json.partial! 'api/v1/factories/complete', factory: @line_product.line.factory
    end
  end
  json.product do
    json.partial! 'api/v1/products/complete', product: @line_product.product
  end
  json.work_calendars @line_product.work_calendars do |work_calendar|
    json.partial! 'api/v1/work_calendars/complete', work_calendar: work_calendar
  end
end
