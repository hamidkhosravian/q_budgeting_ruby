# frozen_string_literal: true

json.id line.id
json.code line.code
json.name line.name
json.start_date line.start_from
json.end_date line.end_from
json.description line.description

json.factory do
  json.partial! 'api/v1/factories/complete', factory: line.factory
end

json.budget_period do
  json.partial! 'api/v1/budget_periods/complete', budget_period: line.budget_period
end
