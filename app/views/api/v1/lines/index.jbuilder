# frozen_string_literal: true

json.lines @lines do |line|
  json.partial! 'complete', line: line
end
