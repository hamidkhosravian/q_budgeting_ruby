# frozen_string_literal: true

json.line do
  json.partial! 'complete', line: @line
end
