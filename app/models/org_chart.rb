class OrgChart < ApplicationRecord
  has_ancestry
  # Associations
  belongs_to :user
  has_one :profile, through: :user
end
