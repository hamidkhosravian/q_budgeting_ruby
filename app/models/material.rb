class Material < ApplicationRecord
  has_ancestry
  # Validations
  validates :name, presence: true
  validates :code, presence: true, uniqueness: true
  # Associations
  belongs_to :budget_period
end
