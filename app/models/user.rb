# frozen_string_literal: true

class User < ApplicationRecord
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i.freeze

  # Validations
  validates :encrypted_password, presence: true
  validates :email, presence: true, uniqueness: { case_sensitive: false }, format: { with: VALID_EMAIL_REGEX }

  # Associations
  has_one :profile, dependent: :destroy
  has_many :auth_tokens, autosave: true, dependent: :destroy
  accepts_nested_attributes_for :profile
  # Enums
  enum role: %i[regular admin]

  # Callbacks
  before_save { email.downcase! }
  before_create :build_default_profile

  # Public Methods
  def valid_password?(password)
    BCrypt::Password.new(encrypted_password) == password
  end

  def password=(password)
    raise BadRequestError, I18n.t('user.sign_up.errors.password_length', minimum: 8) if password.size < 8

    self.encrypted_password = BCrypt::Password.create(password) if password.present?
  end

  def token
    auth_tokens.newer.first
  end

  private

  def build_default_profile
    build_profile if profile.nil?
  end
end
