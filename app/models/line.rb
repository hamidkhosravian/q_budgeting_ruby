class Line < ApplicationRecord
  # validations
  validates_uniqueness_of :code, presence: true
  validates :name, presence: true
  validates :start_from, presence: true
  validates :end_from, presence: true

  # assosiations
  belongs_to :factory
  belongs_to :budget_period
  has_many :line_products
  has_many :products, through: :line_products
  has_many :work_calendars, through: :line_products
end
