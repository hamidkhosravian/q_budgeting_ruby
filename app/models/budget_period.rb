# frozen_string_literal: true

class BudgetPeriod < ApplicationRecord
  # Validations
  validates :code, presence: true
  validates :name, presence: true
  validates :start_date, presence: true
  validates :end_date, presence: true
  validate :no_overlapping

  # assosiations
  has_many :lines
  has_many :materials
  has_many :products
  has_many :announcements
  has_many :sales_channel, through: :announcements

  def no_overlapping
    exists = BudgetPeriod.where('(start_date <= ?) and (end_date >= ?)', start_date, end_date)
                         .or(BudgetPeriod.where('(start_date > ?) and (start_date <= ?)', start_date, end_date))
                         .or(BudgetPeriod.where('(end_date >= ?) and (end_date < ?)', start_date, end_date))
                         .or(BudgetPeriod.where('(start_date > ?) and (end_date < ?)', start_date, end_date))
    if (new_record? && exists.any?) ||
       (persisted? && exists.where.not(id: id).any?)

      errors.add(:end_date, I18n.t('budget_period.errors.overlapping'))
    end
  end
end
