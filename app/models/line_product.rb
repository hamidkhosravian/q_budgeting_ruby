class LineProduct < ApplicationRecord
  # Associations
  belongs_to :line
  belongs_to :product
  has_many :work_calendars
  accepts_nested_attributes_for :work_calendars
end
