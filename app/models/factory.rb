class Factory < ApplicationRecord
  # validations
  validates_uniqueness_of :code, presence: true
  validates :name, presence: true

  # assosiations
  has_many :lines
end
