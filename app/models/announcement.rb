class Announcement < ApplicationRecord
  # Associations
  belongs_to :budget_period
  belongs_to :sales_channel
end
