class Product < ApplicationRecord
  has_ancestry
  # Validations
  validates :name, presence: true
  validates :code, presence: true, uniqueness: true
  # Enum
  enum product_type: %i[product_group product sku]
  # Associations
  belongs_to :budget_period
  has_many :line_products
  has_many :lines, through: :line_products
  has_many :work_calendars, through: :line_products
end
