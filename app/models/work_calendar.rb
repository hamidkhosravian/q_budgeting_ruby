class WorkCalendar < ApplicationRecord
  # Associations
  belongs_to :line_product
  has_one :line, through: :line_product
  has_one :product, through: :line_product

  # Validations
  validate :between_two_dates_of_line
  validate :can_not_duplicate

  def between_two_dates_of_line
    errors.add(:month, I18n.t('work_calendar.errors.out_of_range')) if line.start_from > month ||
                                                                       line.end_from < month
  end

  def can_not_duplicate
    work_calendar = WorkCalendar.arel_table
    exist = WorkCalendar.exists?(work_calendar[:month].between(line.start_from..line.end_from))
    errors.add(:month, I18n.t('work_calendar.errors.duplicate')) if exist
  end

  # Scopes
  scope :group_by_month, -> {
                           select("date_trunc( 'month', month ) as month, sum(maximum_production_capacity) as total_capacity").group('month')
                         }

  # Public Methods
  def number_of_working_days_production
    number_of_working_days.present? ? number_of_working_days : calc_number_of_working_days
  end

  def calc_number_of_working_days
    days_of_month.to_i - legal_holidays.to_i - factory_holidays.to_i - technical_holidays.to_i - sum_wk_other_lines.to_i
  end

  def working_hours_of_month
    number_of_shift_hours.to_i * number_of_working_days.to_i
  end

  def sum_wk_other_lines
    # line_table = Line.arel_table
    line.work_calendars.where.not(number_of_working_days: nil).sum(:number_of_working_days)
    #  .where(line_table[:start_from].gteq(start_from))
    #  .where(line_table[:end_from].lteq(end_from))
  end

  def maximum_hourly_capacity
    maximum_production_capacity.to_i * working_hours_of_month.to_i / 1000
  end

  def suggestion_capacity
    # rubocop:disable Style/AsciiComments
    # TODO: WHAT? مجموع تمام عملیات هر ماه. میخوایم بگیم مجموع ظرفیت هر شیفت چقدره؟؟؟؟؟
    # مجموع ظرفیت های ماهانه هر ماه
    # rubocop:enable Style/AsciiComments
  end

  def maximum_shift_capacity
    suggestion_capacity.to_i * number_of_shift_hours.to_i / 1000
  end

  def average_working_hours_daily
    working_hours_of_month.to_i / number_of_working_days_production.to_i
  end

  def sell_price_per_year
    avrage_price_per_kg * suggestion_capacity / 1000
  end
end
