class SalesChannel < ApplicationRecord
  # Validations
  validates :name, presence: true, uniqueness: { case_sensitive: false }
  # Associations
  has_many :announcements
  has_many :budget_periods, through: :announcements
end
