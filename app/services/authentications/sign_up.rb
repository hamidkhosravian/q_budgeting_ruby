# frozen_string_literal: true

module Authentications
  class SignUp
    include Peafowl

    attribute :registerEmail, String
    attribute :registerPassword, String
    attribute :registerConfirmPassword, String

    validates :registerEmail, presence: true
    validates :registerPassword, presence: true
    validates :registerConfirmPassword, presence: true

    def call
      add_error!(I18n.t('user.sign_up.errors.password_length', minimum: 8)) unless registerPassword.size >= 8
      add_error!(I18n.t('user.sign_up.errors.confirm_password')) unless registerPassword.eql? registerConfirmPassword

      user = ::User.create!(email: registerEmail, password: registerPassword)
      context[:user] = user
    end
  end
end
