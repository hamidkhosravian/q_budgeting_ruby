# frozen_string_literal: true

module Api
  module V1
    module Lines
      class Create
        include Peafowl

        attribute :code, ::String
        attribute :name, ::String
        attribute :start_date, ::DateTime
        attribute :end_date, ::DateTime
        attribute :description, ::String
        attribute :factory_id, ::Integer
        attribute :budget_period_id, ::Integer

        validates :code, presence: true
        validates :name, presence: true
        validates :start_date, presence: true
        validates :end_date, presence: true
        validates :factory_id, presence: true
        validates :budget_period_id, presence: true

        def call
          factory = ::Factory.find(factory_id)
          budget_period = ::BudgetPeriod.find(budget_period_id)
          line = ::Line.create(code: code,
                               name: name,
                               start_from: start_date,
                               end_from: end_date,
                               description: description,
                               factory: factory,
                               budget_period: budget_period)
          return add_error!(line.errors) unless line.valid?

          context[:line] = line
        end
      end
    end
  end
end
