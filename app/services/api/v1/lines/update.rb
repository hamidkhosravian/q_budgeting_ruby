# frozen_string_literal: true

module Api
  module V1
    module Lines
      class Update
        include Peafowl

        attribute :line, ::Line
        attribute :code, ::String
        attribute :name, ::String
        attribute :start_date, ::DateTime
        attribute :end_date, ::DateTime
        attribute :description, ::String
        attribute :factory_id, ::Integer
        attribute :budget_period_id, ::Integer

        def call # rubocop:disable Metrics/CyclomaticComplexity
          factory = ::Factory.find(factory_id) if factory_id.present?
          budget_period = ::BudgetPeriod.find(budget_period_id) if budget_period_id.present?
          line.factory = factory if factory.present?
          line.budget_period = budget_period if budget_period.present?
          line.code = code if code.present?
          line.name = name if name.present?
          line.start_from = start_date if start_date.present?
          line.end_from = end_date if end_date.present?
          line.description = description if description.present?
          return add_error!(line.errors) unless line.save

          context[:line] = line
        end
      end
    end
  end
end
