# frozen_string_literal: true

module Api
  module V1
    module Factories
      class Update
        include Peafowl

        attribute :factory, ::Factory
        attribute :code, ::String
        attribute :name, ::String
        attribute :city, ::String
        attribute :state, ::String
        attribute :postal_code, ::String
        attribute :phone_number, ::String
        attribute :address, ::String
        attribute :description, ::String

        validates :code, presence: true
        validates :name, presence: true

        def call
          factory.code = code
          factory.name = name
          factory.city = city
          factory.state = state
          factory.postal_code = postal_code
          factory.phone_number = phone_number
          factory.address = address
          factory.description = description
          return add_error!(factory.errors) unless factory.save

          context[:factory] = factory
        end
      end
    end
  end
end
