# frozen_string_literal: true

module Api
  module V1
    module Factories
      class Create
        include Peafowl

        attribute :code, ::String
        attribute :name, ::String
        attribute :city, ::String
        attribute :state, ::String
        attribute :postal_code, ::String
        attribute :phone_number, ::String
        attribute :address, ::String
        attribute :description, ::String

        validates :code, presence: true
        validates :name, presence: true

        def call
          factory = ::Factory.create(code: code,
                                     name: name,
                                     city: city,
                                     state: state,
                                     postal_code: postal_code,
                                     phone_number: phone_number,
                                     address: address,
                                     description: description)
          return add_error!(factory.errors) unless factory.valid?

          context[:factory] = factory
        end
      end
    end
  end
end
