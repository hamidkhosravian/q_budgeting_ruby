# frozen_string_literal: true

module Api
  module V1
    module Products
      class Create
        include Peafowl

        attribute :code, ::String
        attribute :name, ::String
        attribute :description, ::String
        attribute :product_type, ::String
        attribute :parent_id, Integer
        attribute :budget_period_id, Integer

        validates :code, presence: true
        validates :name, presence: true
        validates :product_type, inclusion: { in: Product.product_types.keys }
        validates :budget_period_id, presence: true

        def call
          parent = Product.find(parent_id) if parent_id.present?
          budget_period = ::BudgetPeriod.find(budget_period_id)
          product = ::Product.create(code: code,
                                     name: name,
                                     product_type: product_type,
                                     parent: parent,
                                     budget_period: budget_period,
                                     description: description)
          return add_error!(product.errors) unless product.valid?

          context[:product] = product
        end
      end
    end
  end
end
