# frozen_string_literal: true

module Api
  module V1
    module Products
      class Update
        include Peafowl

        attribute :product, ::Product
        attribute :code, ::String
        attribute :name, ::String
        attribute :description, ::String
        attribute :product_type, ::String
        attribute :parent_id, Integer
        attribute :budget_period_id, Integer

        validates :product, presence: true
        validates :code, presence: true
        validates :name, presence: true
        validates :product_type, inclusion: { in: Product.product_types.keys }

        def call
          parent = Product.find(parent_id) if parent_id.present?

          if budget_period_id.present?
            budget_period = ::BudgetPeriod.find(budget_period_id)
            product.budget_period = budget_period
          end

          product.code = code
          product.name = name
          product.product_type = product_type
          product.parent = parent
          product.description = description
          return add_error!(product.errors) unless product.save

          context[:product] = product
        end
      end
    end
  end
end
