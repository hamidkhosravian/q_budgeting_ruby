# frozen_string_literal: true

module Api
  module V1
    module Materials
      class Create
        include Peafowl

        attribute :code, ::String
        attribute :name, ::String
        attribute :description, ::String
        attribute :parent_id, Integer
        attribute :budget_period_id, Integer

        validates :code, presence: true
        validates :name, presence: true
        validates :budget_period_id, presence: true

        def call
          parent = ::Material.find(parent_id) if parent_id.present?
          budget_period = ::BudgetPeriod.find(budget_period_id)
          material = ::Material.create(code: code,
                                       name: name,
                                       parent: parent,
                                       budget_period: budget_period,
                                       description: description)
          return add_error!(material.errors) unless material.valid?

          context[:material] = material
        end
      end
    end
  end
end
