# frozen_string_literal: true

module Api
  module V1
    module Materials
      class Update
        include Peafowl

        attribute :material, ::Material
        attribute :code, ::String
        attribute :name, ::String
        attribute :description, ::String
        attribute :parent_id, Integer
        attribute :budget_period_id, Integer

        validates :material, presence: true
        validates :code, presence: true
        validates :name, presence: true

        def call
          parent = Material.find(parent_id) if parent_id.present?

          if budget_period_id.present?
            budget_period = ::BudgetPeriod.find(budget_period_id)
            material.budget_period = budget_period
          end

          material.code = code
          material.name = name
          material.parent = parent
          material.description = description
          return add_error!(material.errors) unless material.save

          context[:material] = material
        end
      end
    end
  end
end
