# frozen_string_literal: true

module Api
  module V1
    module BudgetPeriods
      class Update
        include Peafowl

        attribute :budget_period, ::BudgetPeriod
        attribute :code, ::String
        attribute :name, ::String
        attribute :start_date, ::DateTime
        attribute :end_date, ::DateTime
        attribute :description, ::String

        validates :budget_period, presence: true
        validates :code, presence: true
        validates :name, presence: true
        validates :start_date, presence: true
        validates :end_date, presence: true

        def call
          budget_period.code = code
          budget_period.name = name
          budget_period.start_date = start_date
          budget_period.end_date = end_date
          budget_period.description = description
          return add_error!(budget_period.errors) unless budget_period.save

          context[:budget_period] = budget_period
        end
      end
    end
  end
end
