# frozen_string_literal: true

module Api
  module V1
    module BudgetPeriods
      class Create
        include Peafowl

        attribute :code, ::String
        attribute :name, ::String
        attribute :start_date, ::DateTime
        attribute :end_date, ::DateTime
        attribute :description, ::String

        validates :code, presence: true
        validates :name, presence: true
        validates :start_date, presence: true
        validates :end_date, presence: true

        def call
          budget_period = ::BudgetPeriod.create(code: code,
                                                name: name,
                                                start_date: start_date,
                                                end_date: end_date,
                                                description: description)
          return add_error!(budget_period.errors) unless budget_period.valid?

          context[:budget_period] = budget_period
        end
      end
    end
  end
end
