# frozen_string_literal: true

module Api
  module V1
    class FactoriesController < ApiController
      before_action :authenticate_user_from_token!
      before_action :find_source, only: %i[show update destroy]

      def index
        @factories = Factory.all
        render :index
      end

      def create
        result = ::Api::V1::Factories::Create.call(permitted)
        return render json: { errors: result.errors }, status: :bad_request if result.failure?

        @factory = result.factory
        render :show, status: :created
      end

      def show
        render :show
      end

      def update
        result = ::Api::V1::Factories::Update.call(permitted.merge(factory: @factory))
        return render json: { errors: result.errors }, status: :bad_request if result.failure?

        @factory = result.factory
        render :show
      end

      def destroy
        @factory.destroy!
      end

      private

      def permitted
        params.permit(
          :code,
          :name,
          :city,
          :state,
          :postal_code,
          :phone_number,
          :address,
          :description
        )
      end

      def find_source
        @factory = Factory.find(params[:id])
      end
    end
  end
end
