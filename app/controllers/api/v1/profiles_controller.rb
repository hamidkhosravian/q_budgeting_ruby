# frozen_string_literal: true

module Api
  module V1
    class ProfilesController < ApiController
      before_action :authenticate_user_from_token!
      before_action :set_profile, only: %i[show update]

      def index
        @profiles = Profile.all
        render 'api/v1/profiles/index', status: :ok
      end

      def destroy
        @profile = Profile.find(params[:id])
        @profile.user.destroy!
      end

      def show
        @profile = current_user.profile
      end

      def update
        if @profile.update(profile_params)
          render :show, status: :ok
        else
          render json: @profile.errors, status: :unprocessable_entity
        end
      end

      private

      def set_profile
        @profile = current_user.profile
      end

      def profile_params
        params.permit(:avatar,
                      :personal_code,
                      :national_code,
                      :first_name,
                      :last_name,
                      :city,
                      :state,
                      :postal_code,
                      :address,
                      :phone_number,
                      :description,
                      :hired_at)
      end
    end
  end
end
