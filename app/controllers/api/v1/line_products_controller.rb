# frozen_string_literal: true

module Api
  module V1
    class LineProductsController < ApiController
      before_action :authenticate_user_from_token!
      before_action :set_line_product, only: %i[show update destroy]
      before_action :set_line_and_product, only: %i[create update]

      def index
        @line_products = LineProduct.all
        if params[:line_id].present?
          line = Line.find(params[:line_id])
          @line_products = @line_products.where(line: line)
        end

        @line_products = @line_products.includes(:line).order('lines.id ASC')

        render :index
      end

      def show
        render :show
      end

      def create
        @line_product = LineProduct.find_or_initialize_by(product: @product, line: @line)
        @line_product.assign_attributes(line_product_params)
        @line_product.save!

        @line_products = LineProduct.all

        if params[:line_id].present?
          line = Line.find(params[:line_id])
          @line_products = @line_products.where(line: line)
        end

        render :index
      end

      def update
        @line_product.update_attributes!(line_product_params.merge!(product: @product, line: @line))
        render :show
      end

      def destroy
        @line_product.destroy!

        @line_products = LineProduct.all

        if params[:line_id].present?
          line = Line.find(params[:line_id])
          @line_products = @line_products.where(line: line)
        end

        render :index
      end

      def productive_power_monthly; end

      private

      def set_line_product
        @line_product = LineProduct.find(params[:id])
      end

      def set_line_and_product
        @product = Product.find(params[:product_id])
        @line = Line.find(params[:line_id])
      end

      def line_product_params
        params.permit(work_calendars_attributes: [%i[id
                                                     month
                                                     days_of_month
                                                     legal_holidays
                                                     factory_holidays
                                                     technical_holidays
                                                     number_of_working_days
                                                     number_of_shift_hours
                                                     minimum_production_capacity
                                                     maximum_production_capacity]])
      end
    end
  end
end
