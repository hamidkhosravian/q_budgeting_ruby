# frozen_string_literal: true

module Api
  module V1
    class OrgChartsController < ApiController
      before_action :authenticate_user_from_token!

      def index
        if params[:parent_id].present?
          parent = OrgChart.find(params[:parent_id])
          @roots = OrgChart.where(parent: parent)
        else
          @roots = OrgChart.roots
        end
        render :index
      end

      def create
        parent = OrgChart.find(params[:parent_id]) if params[:parent_id]
        profile = Profile.find_by!(personal_code: params[:personal_code])
        org_chart = OrgChart.new
        org_chart.user = profile.user
        org_chart.parent_id = parent.id if parent.present?
        org_chart.save!
        @roots = OrgChart.roots
        render :index
      end

      def update
        org_chart = OrgChart.find(params[:parent_id])
        if params[:personal_code]
          profile = Profile.find_by!(personal_code: params[:personal_code])
          org_chart.user = profile.user
        end
        org_chart.save!

        @roots = OrgChart.roots
        render :index
      end

      def destroy
        org_chart = OrgChart.find(params[:id])
        org_chart.destroy!

        @roots = OrgChart.roots
        render :index
      end
    end
  end
end
