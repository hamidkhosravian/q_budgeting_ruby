# frozen_string_literal: true

module Api
  module V1
    class ProductsController < ApiController
      before_action :authenticate_user_from_token!
      before_action :set_product, only: %i[show update destroy]

      def index
        @products = Product.roots

        if params[:budget_period].present?
          budget_period = BudgetPeriod.find(params[:budget_period])
          @products = @products.where(budget_period: budget_period)
        end

        if params[:parent_id].present?
          parent = @products.find(params[:parent_id])
          @products = @products.where(parent: parent)
        end

        render :index
      end

      def show
        render :show
      end

      def create
        result = ::Api::V1::Products::Create.call(product_params)
        return render json: { errors: result.errors }, status: :bad_request if result.failure?

        @products = Product.roots

        if params[:budget_period].present?
          budget_period = BudgetPeriod.find(params[:budget_period])
          @products = @products.where(budget_period: budget_period)
        end

        render :index, status: :created
      end

      def update
        result = ::Api::V1::Products::Update.call(product_params.merge(product: @product))
        return render json: { errors: result.errors }, status: :bad_request if result.failure?

        @product = result.product
        render :show
      end

      def destroy
        @product.destroy!

        @products = Product.roots

        if params[:budget_period].present?
          budget_period = BudgetPeriod.find(params[:budget_period])
          @products = @products.where(budget_period: budget_period)
        end

        render :index
      end

      private

      def set_product
        @product = Product.find(params[:id])
      end

      def product_params
        params.permit(:name, :code, :description, :product_type, :parent_id, :budget_period_id)
      end
    end
  end
end
