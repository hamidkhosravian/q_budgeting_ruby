# frozen_string_literal: true

module Api
  module V1
    class AuthenticationsController < ApiController
      def sign_up
        ActiveRecord::Base.transaction do
          result = Authentications::SignUp.call(params.permit(
                                                  :registerEmail,
                                                  :registerPassword,
                                                  :registerConfirmPassword
                                                ))
          raise BadRequestError, result.errors if result.failure?

          result.user.profile.update!(profile_params)
        end

        render json: { response: I18n.t('user.sign_up.successful'), status: 201 }, status: :created
      end

      def sign_in
        result = Authentications::SignIn.call(params.permit(
                                                :email,
                                                :password
                                              ))
        raise BadRequestError, result.errors if result.failure?

        @user = result.user
        render 'api/v1/users/show', status: :ok
      end

      private

      def profile_params
        params.permit(:avatar,
                      :personal_code,
                      :national_code,
                      :first_name,
                      :last_name,
                      :city,
                      :state,
                      :postal_code,
                      :address,
                      :phone_number,
                      :description,
                      :hired_at)
      end
    end
  end
end
