# frozen_string_literal: true

module Api
  module V1
    class AnnouncementsController < ApiController
      before_action :authenticate_user_from_token!
      before_action :find_source, only: %i[show update destroy]

      def index
        @announcements = Announcement.all
        render :index
      end

      def create
        @announcement = Announcement.create!(permitted)
        render :show, status: :created
      end

      def show
        render :show
      end

      def update
        @announcement.update_attributes(permitted)
        render :show
      end

      def destroy
        @announcement.destroy!
      end

      private

      def permitted
        params.permit(:total_percentage).tap do |announce|
          announce[:budget_period] = BudgetPerion.find(params[:budget_period_id])
          announce[:sales_channel_id] = SalesChannel.find(params[:sales_channel_id])
        end
      end

      def find_source
        @announcement = Announcement.find(params[:id])
      end
    end
  end
end
