# frozen_string_literal: true

module Api
  module V1
    class LinesController < ApiController
      before_action :authenticate_user_from_token!
      before_action :find_source, only: %i[show update destroy]

      def index
        @lines = Line.all
        @lines = @lines.where(budget_period_id: params[:budget_period_id]) if params[:budget_period_id].present?
        @lines = @lines.where(factory_id: params[:factory_id]) if params[:factory_id].present?

        render :index
      end

      def create
        result = ::Api::V1::Lines::Create.call(permitted)
        return render json: { errors: result.errors }, status: :bad_request if result.failure?

        @line = result.line
        render :show, status: :created
      end

      def show
        render :show
      end

      def update
        result = ::Api::V1::Lines::Update.call(permitted.merge(line: @line))
        return render json: { errors: result.errors }, status: :bad_request if result.failure?

        @line = result.line
        render :show
      end

      def destroy
        @line.destroy!
      end

      private

      def permitted
        params.permit(
          :code,
          :name,
          :start_date,
          :end_date,
          :description,
          :factory_id,
          :budget_period_id
        )
      end

      def find_source
        @line = Line.find(params[:id])
      end
    end
  end
end
