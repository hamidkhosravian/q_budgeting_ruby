# frozen_string_literal: true

module Api
  module V1
    class SalesChannelsController < ApiController
      before_action :authenticate_user_from_token!
      before_action :find_source, only: %i[show update destroy]

      def index
        @sales_channels = SalesChannel.all
        render :index
      end

      def create
        @sales_channel = SalesChannel.create!(name: params[:name])
        render :show, status: :created
      end

      def show
        render :show
      end

      def update
        @sales_channel.update_attributes(name: params[:name])
        render :show
      end

      def destroy
        @sales_channel.destroy!
      end

      private

      def find_source
        @sales_channel = SalesChannel.find(params[:id])
      end
    end
  end
end
