# frozen_string_literal: true

module Api
  module V1
    class BudgetPeriodsController < ApiController
      before_action :authenticate_user_from_token!
      before_action :find_source, only: %i[show update destroy]

      def index
        @budget_periods = BudgetPeriod.all
        render :index
      end

      def create
        result = ::Api::V1::BudgetPeriods::Create.call(permitted)
        return render json: { errors: result.errors }, status: :bad_request if result.failure?

        @budget_period = result.budget_period
        render :show, status: :created
      end

      def show
        render :show
      end

      def update
        result = ::Api::V1::BudgetPeriods::Update.call(permitted.merge(budget_period: @budget_period))
        return render json: { errors: result.errors }, status: :bad_request if result.failure?

        @budget_period = result.budget_period
        render :show
      end

      def destroy
        @budget_period.destroy!
      end

      private

      def permitted
        params.permit(
          :code,
          :name,
          :start_date,
          :end_date,
          :description
        )
      end

      def find_source
        @budget_period = BudgetPeriod.find(params[:id])
      end
    end
  end
end
