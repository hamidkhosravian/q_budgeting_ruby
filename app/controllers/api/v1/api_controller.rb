# frozen_string_literal: true

module Api
  module V1
    class ApiController < ApplicationController
      protected

      # authenticate user from Authorization in header with exception
      def authenticate_user_from_token!
        raise AuthorizationError unless http_auth_token.present?

        result = find_user
        raise BadRequestError, result.errors if result.failure?

        @current_user = result.current_user
      end

      # we use this method for pundit
      attr_reader :current_user

      private

      def find_user
        result = Authentications::Authenticate.call(auth_token: http_auth_token)
      end

      # get token from Authorization in header
      def http_auth_token
        auth_token = request.env['HTTP_AUTHORIZATION']
        auth_token.split(' ').last if auth_token.present? && auth_token.split(' ').first.casecmp('bearer').zero?
      end
    end
  end
end
