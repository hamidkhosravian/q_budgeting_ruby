# frozen_string_literal: true

module Api
  module V1
    class ProductivePowerController < ApplicationController
      before_action :find_source, only: [:index]
      def index
        @range_dates = products_per_months
        @products = Product.includes(:budget_period, line_products: [:work_calendars])
                           .references(:budget_period, line_products: [:work_calendars])
                           .where(budget_periods: { id: @budget_period.id })
                           .where(work_calendars: {
                                    month: [@budget_period.start_date.beginning_of_day..@budget_period.end_date.end_of_day]
                                  })

        render :index
      end

      def show; end

      def find_source
        @budget_period = BudgetPeriod.find(params[:budget_period_id])
      end

      def products_per_months
        from = @budget_period.start_date.at_beginning_of_month
        to = @budget_period.end_date.at_beginning_of_month
        per_months = [from]
        months_count = (to.year * 12 + to.month) - (from.year * 12 + from.month)
        months_count.times do
          from = from.next_month
          per_months << from
        end
        per_months
      end
    end
  end
end
