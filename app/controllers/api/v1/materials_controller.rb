# frozen_string_literal: true

module Api
  module V1
    class MaterialsController < ApiController
      before_action :authenticate_user_from_token!
      before_action :set_material, only: %i[show update destroy]

      def index
        @materials = Material.roots

        if params[:budget_period].present?
          budget_period = BudgetPeriod.find(params[:budget_period])
          @materials = @materials.where(budget_period: budget_period)
        end

        if params[:parent_id].present?
          parent = @materials.find(params[:parent_id])
          @materials = @materials.where(parent: parent)
        end

        render :index
      end

      def show
        render :show
      end

      def create
        result = ::Api::V1::Materials::Create.call(material_params)
        return render json: { errors: result.errors }, status: :bad_request if result.failure?

        @materials = Material.roots

        if params[:budget_period].present?
          budget_period = BudgetPeriod.find(params[:budget_period])
          @materials = @materials.where(budget_period: budget_period)
        end

        render :index, status: :created
      end

      def update
        result = ::Api::V1::Materials::Update.call(material_params.merge(material: @material))
        return render json: { errors: result.errors }, status: :bad_request if result.failure?

        @material = result.material
        render :show
      end

      def destroy
        @material.destroy!

        @materials = Material.roots

        if params[:budget_period].present?
          budget_period = BudgetPeriod.find(params[:budget_period])
          @materials = @materials.where(budget_period: budget_period)
        end

        render :index
      end

      private

      def set_material
        @material = Material.find(params[:id])
      end

      def material_params
        params.permit(:name, :code, :description, :parent_id, :budget_period_id)
      end
    end
  end
end
