# frozen_string_literal: true

Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      resource :authentications, only: [], path: '' do
        collection do
          post :sign_up
          post :sign_in
        end
      end
      resources :org_charts, only: %i[index create update destroy]
      resources :budget_periods
      resources :products
      resources :materials
      resources :line_products
      resources :lines do
        resources :products, only: [:index]
      end
      get 'profile', to: 'profiles#show'
      put 'profile', to: 'profiles#update'
      resources :profiles, only: %i[index destroy]
      resources :factories
      resources :productive_power, only: %i[index show]
      resources :sales_channel
      resources :announcements
    end
  end
end
