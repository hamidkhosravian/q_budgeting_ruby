require 'rails_helper'

RSpec.describe Api::V1::ProfilesController, type: :routing do
  describe 'routing' do
    it 'routes to #show' do
      expect(get: '/api/v1/profile').to route_to(controller: 'api/v1/profiles', action: 'show')
    end

    it 'routes to #update via PUT' do
      expect(put: '/api/v1/profile').to route_to(controller: 'api/v1/profiles', action: 'update')
    end
  end
end
