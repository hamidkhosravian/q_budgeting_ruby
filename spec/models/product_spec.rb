require 'rails_helper'

RSpec.describe Product, type: :model do
  context 'Validations' do
    it { is_expected.to validate_presence_of :name }
    it { is_expected.to validate_uniqueness_of :code }
  end

  context 'associations' do
    it { is_expected.to belong_to :budget_period }
    it { is_expected.to have_many(:line_products) }
    it { is_expected.to have_many(:lines) }
  end
end
