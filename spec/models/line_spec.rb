require 'rails_helper'

RSpec.describe Line, type: :model do
  context 'validations' do
    it { is_expected.to validate_uniqueness_of :code }
    it { is_expected.to validate_presence_of :name }
    it { is_expected.to validate_presence_of :start_from }
    it { is_expected.to validate_presence_of :end_from }
  end

  context 'associations' do
    it { is_expected.to belong_to(:factory) }
    it { is_expected.to belong_to(:budget_period) }
    it { is_expected.to have_many(:line_products) }
    it { is_expected.to have_many(:products) }
  end
end
