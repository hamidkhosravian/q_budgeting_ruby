require 'rails_helper'

RSpec.describe WorkCalendar, type: :model do
  context 'associations' do
    it { is_expected.to belong_to(:line_product) }
    it { is_expected.to have_one(:line) }
    it { is_expected.to have_one(:product) }
  end
end
