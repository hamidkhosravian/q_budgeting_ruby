require 'rails_helper'

RSpec.describe Material, type: :model do
  context 'Validations' do
    it { is_expected.to validate_presence_of :name }
    it { is_expected.to validate_uniqueness_of :code }
  end

  context 'Associations' do
    it { is_expected.to belong_to :budget_period }
  end
end
