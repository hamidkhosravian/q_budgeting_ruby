require 'rails_helper'

RSpec.describe LineProduct, type: :model do
  context 'associations' do
    it { is_expected.to belong_to(:line) }
    it { is_expected.to belong_to(:product) }
    it { is_expected.to have_many(:work_calendars) }
  end
end
