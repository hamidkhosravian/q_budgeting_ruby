# frozen_string_literal: true

require 'rails_helper'

RSpec.describe BudgetPeriod, type: :model do
  context 'Validations' do
    it { is_expected.to validate_presence_of :code }
    it { is_expected.to validate_presence_of :name }
    it { is_expected.to validate_presence_of :start_date }
    it { is_expected.to validate_presence_of :end_date }
  end

  context 'associations' do
    it { is_expected.to have_many(:lines) }
    it { is_expected.to have_many(:materials) }
    it { is_expected.to have_many(:products) }
  end
end
