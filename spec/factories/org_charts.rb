FactoryBot.define do
  factory :org_chart do
    user
    position { Faker::Job.position }
  end
end
