FactoryBot.define do
  factory :work_calendar do
    line_product
    month { Time.zone.now }
    days_of_month { Faker::Number.number(2) }
    legal_holidays { Faker::Number.number(2) }
    factory_holidays { Faker::Number.number(2) }
    technical_holidays { Faker::Number.number(2) }
    number_of_working_days { Faker::Number.number(2) }
    number_of_shift_hours { Faker::Number.number(2) }
    maximum_production_capacity { Faker::Number.number(2) }
  end
end
