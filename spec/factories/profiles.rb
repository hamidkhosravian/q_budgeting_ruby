FactoryBot.define do
  factory :profile do
    user
    avatar { Rack::Test::UploadedFile.new(Rails.root.join('spec', 'fixtures', 'images', 'avatar.jpg')) }
    personal_code { Faker::Code.ean }
    national_code { Faker::Code.ean }
    first_name { Faker::Name.first_name }
    last_name { Faker::Name.last_name }
    city { Faker::Address.city }
    state { Faker::Address.state }
    postal_code { Faker::Address.postcode }
    address { Faker::Address.full_address }
    phone_number { '09101234123' }
    description { Faker::Lorem.paragraph }
    hired_at { Time.zone.now.to_date.to_s }
  end
end
