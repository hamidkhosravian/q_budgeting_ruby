# frozen_string_literal: true

FactoryBot.define do
  factory :user, class: 'User' do
    email { Faker::Internet.email }
    password { '123456aA' }
    role { 'regular' }
    factory :valid_user_register do
      confirm_password { '123456aA' }
    end
    trait(:profile) do
      profile_attributes { attributes_for(:profile) }
    end
  end
end
