FactoryBot.define do
  factory :announcement do
    budget_period { nil }
    sales_channel { nil }
    total_percentage { '9.99' }
  end
end
