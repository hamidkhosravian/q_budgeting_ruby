# frozen_string_literal: true

FactoryBot.define do
  factory :auth_token, class: 'AuthToken' do
    token { SecureRandom.hex }
    refresh_token { SecureRandom.hex }
    user
  end
end
