FactoryBot.define do
  factory :product do
    budget_period
    code { Faker::Company.duns_number }
    name { Faker::Company.name }
    description { Faker::Lorem.paragraph }
    product_type { Product.product_types.keys.sample }
    trait :product_group do
      product_type { :product_group }
    end
    trait :product do
      product_type { :product }
    end
    trait :sku do
      product_type { :sku }
    end
  end
end
