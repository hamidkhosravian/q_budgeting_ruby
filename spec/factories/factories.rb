FactoryBot.define do
  factory :factory do
    code { Faker::Company.duns_number }
    name { Faker::Company.name }
    city { Faker::Address.city }
    state { Faker::Address.state }
    postal_code { Faker::Address.postcode }
    address { Faker::Address.full_address }
    phone_number { '09101234123' }
    description { Faker::Lorem.paragraph }
  end
end
