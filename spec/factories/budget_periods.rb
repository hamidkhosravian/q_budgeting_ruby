# frozen_string_literal: true

FactoryBot.define do
  factory :budget_period do
    code { Faker::Code.ean }
    name { Faker::Name.name }
    sequence(:start_date) { |cnt| Time.zone.now.change(month: (cnt % 12) + 1, day: 1).to_date.to_s }
    sequence(:end_date) { |cnt| Time.zone.now.change(month: (cnt % 12) + 1, day: 20).to_date.to_s }
    description { Faker::Lorem.paragraph }
  end
end
