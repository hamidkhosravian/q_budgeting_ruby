FactoryBot.define do
  factory :material do
    budget_period
    code { Faker::Company.duns_number }
    name { Faker::Company.name }
    description { Faker::Lorem.paragraph }
  end
end
