FactoryBot.define do
  factory :line do
    code { Faker::Company.duns_number }
    name { Faker::Company.name }
    description { Faker::Lorem.paragraph }
    factory_id { create(:factory).id }
    budget_period
    sequence(:start_from) { |cnt| Time.zone.now.change(month: (cnt % 12) + 1, day: 1).to_date.to_s }
    sequence(:end_from) { |cnt| Time.zone.now.change(month: (cnt % 12) + 1, day: 20).to_date.to_s }
  end
end
