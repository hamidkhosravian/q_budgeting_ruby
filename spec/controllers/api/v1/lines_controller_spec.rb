# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Api::V1::LinesController, type: :controller do
  describe 'GET #index' do
    before do
      create_list(:line, 3)
      sign_in
      get :index
    end

    it 'returns http success' do
      expect(response).to have_http_status(:ok)
    end

    it 'response with JSON body containing expected Job Signature attributes' do
      expect(hash_body.keys).to match_array([:lines])
      expect(hash_body[:lines].size).to eq(3)
      items = %i[id code name start_from end_from description]
      expect(hash_body[:lines].first.keys).to match_array(items)
    end
  end

  describe 'POST #Create' do
    let(:factory) { create(:factory) }
    let(:budget_period) { create(:budget_period) }

    it 'returns http created' do
      sign_in
      post :create, params: attributes_for(:line)
        .merge(factory_id: factory.id,
               budget_period_id: budget_period.id)
      expect(response).to have_http_status(:created)
    end
  end

  describe 'PUT #Update' do
    let(:factory) { create(:factory) }
    let(:budget_period) { create(:budget_period) }
    let(:line) { create(:line) }
    let(:updated) { attributes_for(:line).merge(id: line.id) }

    before do
      sign_in
      put :update, params: updated.merge(factory_id: factory.id,
                                         budget_period_id: budget_period.id)
    end

    it 'returns http success' do
      expect(response).to have_http_status(:ok)
    end
    it 'response with JSON body containing expected Job Signature attributes' do
      expect(hash_body.keys).to match_array([:line])
      items = %i[id code name start_from end_from description]
      expect(hash_body[:line].keys).to match_array(items)
      expect(hash_body[:line]).to match(updated.except(:factory_id))
    end
  end

  describe 'DELETE #Destroy' do
    let(:line) { create(:line) }

    before do
      sign_in
      delete :destroy, params: { id: line.id }
    end

    it 'returns http no content' do
      expect(response).to have_http_status(:no_content)
    end
  end
end
