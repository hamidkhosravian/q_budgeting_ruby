require 'rails_helper'

RSpec.describe Api::V1::ProfilesController, type: :controller do
  let(:user) { create(:user, :profile) }

  describe 'GET #index' do
    before do
      create_list(:user, 3, :profile)
      sign_in(user)
      get :index
    end

    it 'returns a success response' do
      expect(response).to be_successful
    end

    it 'render an array of profiles JSON' do
      expect(hash_body[:profiles].size).to eq(4)
    end
  end

  describe 'GET #show' do
    it 'returns a success response' do
      sign_in(user)
      get :show
      expect(response).to be_successful
    end
  end

  describe 'PUT #update' do
    context 'with valid params' do
      let(:new_attributes) do
        attributes_for(:profile).except(:user)
                                .merge(id: user.to_param, email: user.email)
      end

      it 'updates the requested profile' do
        sign_in(user)
        put :update, params: new_attributes
        expect(response).to have_http_status(:ok)
      end

      it 'renders a JSON response with the profile' do
        sign_in(user)
        put :update, params: new_attributes
        expect(response).to have_http_status(:ok)
        expect(hash_body[:profile]).to match(new_attributes
                                                 .except(:id, :avatar)
                                                 .merge(avatar: Profile.last.avatar.abs_path))
      end
    end
  end

  describe 'DELETE #destroy' do
    it 'returns a success response' do
      sign_in(user)
      another_profile = create(:profile)
      delete :destroy, params: { id: another_profile.id }
      expect(response).to be_successful
    end

    it 'returns the bad request response' do
      sign_in(user)
      delete :destroy, params: { id: user.profile.id }
      expect(response).not_to be_successful
    end
  end
end
