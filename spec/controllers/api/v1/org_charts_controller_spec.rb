# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Api::V1::OrgChartsController, type: :controller do
  describe 'GET #index' do
    before do
      root = FactoryBot.create(:org_chart)
      lnode, rnode = FactoryBot.create_list(:org_chart, 2, parent: root)
      FactoryBot.create_list(:org_chart, 2, parent: lnode)
      FactoryBot.create_list(:org_chart, 2, parent: rnode)
      sign_in
      get :index
    end

    it 'returns http success' do
      expect(response).to have_http_status(:ok)
    end

    it 'response with JSON body containing expected Job Signature attributes' do
      expect(hash_body.keys).to match_array([:roots])
    end
  end
end
