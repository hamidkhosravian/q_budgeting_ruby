require 'rails_helper'

RSpec.describe Api::V1::ProductivePowerController, type: :controller do
  describe 'GET #index' do
    it 'returns http success' do
      budget_period = FactoryBot.create(:budget_period)
      root = FactoryBot.create_list(:product, 10, budget_period: budget_period).each do |product|
        line = FactoryBot.create(:line, budget_period: budget_period)
        FactoryBot.create_list(:line_product, 3, product: product, line: line).each do |line_product|
          month = Faker::Date.between(line.start_from, line.end_from)
          FactoryBot.create_list(:work_calendar, 3, line_product: line_product, month: month)
        end
      end
      get :index, params: { budget_period_id: budget_period.id }
      expect(response).to have_http_status(:success)
    end
  end

  describe 'GET #show' do
    it 'returns http success' do
      get :show
      expect(response).to have_http_status(:success)
    end
  end
end
