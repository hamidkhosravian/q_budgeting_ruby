# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Api::V1::FactoriesController, type: :controller do
  describe 'GET #index' do
    before do
      create_list(:factory, 3)
      sign_in
      get :index
    end

    it 'returns http success' do
      expect(response).to have_http_status(:ok)
    end

    it 'response with JSON body containing expected Job Signature attributes' do
      expect(hash_body.keys).to match_array([:factories])
      expect(hash_body[:factories].size).to eq(3)
      items = %i[id code name city state address postal_code phone_number description]
      expect(hash_body[:factories].first.keys).to match_array(items)
    end
  end

  describe 'POST #Create' do
    it 'returns http created' do
      sign_in
      post :create, params: attributes_for(:factory)
      expect(response).to have_http_status(:created)
    end
  end

  describe 'PUT #Update' do
    let(:factory) { create(:factory) }
    let(:updated) { attributes_for(:factory).merge(id: factory.id) }

    before do
      sign_in
      put :update, params: updated
    end

    it 'returns http success' do
      expect(response).to have_http_status(:ok)
    end
    it 'response with JSON body containing expected Job Signature attributes' do
      expect(hash_body.keys).to match_array([:factory])
      items = %i[id code name city state address postal_code phone_number description]
      expect(hash_body[:factory].keys).to match_array(items)
      expect(hash_body[:factory]).to match(updated)
    end
  end

  describe 'DELETE #Destroy' do
    let(:factory) { create(:factory) }

    before do
      sign_in
      delete :destroy, params: { id: factory.id }
    end

    it 'returns http no content' do
      expect(response).to have_http_status(:no_content)
    end
  end
end
