# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Api::V1::BudgetPeriodsController, type: :controller do
  describe 'GET #index' do
    before do
      create_list(:budget_period, 3)
      sign_in
      get :index
    end

    it 'returns http success' do
      expect(response).to have_http_status(:ok)
    end

    it 'response with JSON body containing expected Job Signature attributes' do
      expect(hash_body.keys).to match_array([:budget_periods])
      expect(hash_body[:budget_periods].size).to eq(3)
      items = %i[id code name start_date end_date description]
      expect(hash_body[:budget_periods].first.keys).to match_array(items)
    end
  end

  describe 'POST #Create' do
    it 'returns http created' do
      sign_in
      post :create, params: attributes_for(:budget_period)
      expect(response).to have_http_status(:created)
    end

    it 'returns http bad request' do
      sign_in
      create(:budget_period, start_date: '2018-01-01', end_date: '2018-01-30')
      post :create, params: attributes_for(:budget_period, start_date: '2018-01-01', end_date: '2018-01-30')
      expect(response).to have_http_status(:bad_request)
    end

    it 'returns http bad request for overlap' do
      sign_in
      create(:budget_period, start_date: '2018-01-01', end_date: '2018-01-30')
      post :create, params: attributes_for(:budget_period, start_date: '2018-01-05', end_date: '2018-01-25')
      expect(response).to have_http_status(:bad_request)
    end

    it 'returns http bad request for another overlap' do
      sign_in
      create(:budget_period, start_date: '2018-01-05', end_date: '2018-01-25')
      post :create, params: attributes_for(:budget_period, start_date: '2018-01-01', end_date: '2018-01-30')
      expect(response).to have_http_status(:bad_request)
    end
  end

  describe 'PUT #Update' do
    let(:budget_period) { create(:budget_period) }
    let(:updated) { attributes_for(:budget_period).merge(id: budget_period.id) }

    before do
      sign_in
      put :update, params: updated
    end

    it 'returns http success' do
      expect(response).to have_http_status(:ok)
    end
    it 'response with JSON body containing expected Job Signature attributes' do
      expect(hash_body.keys).to match_array([:budget_period])
      items = %i[id code name start_date end_date description]
      expect(hash_body[:budget_period].keys).to match_array(items)
      expect(hash_body[:budget_period]).to match(updated)
    end
  end

  describe 'DELETE #Destroy' do
    let(:budget_period) { create(:budget_period) }

    before do
      sign_in
      delete :destroy, params: { id: budget_period.id }
    end

    it 'returns http no content' do
      expect(response).to have_http_status(:no_content)
    end
  end
end
