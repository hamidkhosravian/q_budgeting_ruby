# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Api::V1::MaterialsController, type: :controller do
  describe 'GET #index' do
    before do
      budget_period = create(:budget_period)
      root = create(:material, budget_period: budget_period)
      create_list(:material, 2, parent: root, budget_period: budget_period)
      sign_in
      get :index, params: { budget_period_id: budget_period.id }
    end

    it 'returns http success' do
      expect(response).to have_http_status(:ok)
    end

    it 'response with JSON body containing expected Job Signature attributes' do
      expect(hash_body.keys).to match_array([:materials])
      expect(hash_body[:materials].size).to eq(1)
      items = %i[id code name description children budget_period]
      expect(hash_body[:materials].first.keys).to match_array(items)
    end
  end

  describe 'POST #Create' do
    it 'returns http created' do
      sign_in
      budget_period = create(:budget_period)
      post :create, params: attributes_for(:material).merge(budget_period_id: budget_period.id)
      expect(response).to have_http_status(:created)
    end
  end

  describe 'PUT #Update' do
    let(:material) { create(:material) }
    let(:updated) { attributes_for(:material).merge(id: material.id) }

    before do
      sign_in
      put :update, params: updated
    end

    it 'returns http success' do
      expect(response).to have_http_status(:ok)
    end
  end

  describe 'DELETE #Destroy' do
    let(:material) { create(:material) }

    before do
      sign_in
      delete :destroy, params: { id: material.id }
    end

    it 'returns http no content' do
      expect(response).to have_http_status(:no_content)
    end
  end
end
