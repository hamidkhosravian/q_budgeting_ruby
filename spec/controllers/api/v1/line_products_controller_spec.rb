# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Api::V1::LineProductsController, type: :controller do
  describe 'GET #index' do
    before do
      budget_periods = create_list(:budget_period, 3)
      lines = budget_periods.map { |bp| create(:line, budget_period: bp) }
      lines.map { |line| create(:line_product, line: line) }
      sign_in
      get :index
    end

    it 'returns http success' do
      expect(response).to have_http_status(:ok)
    end

    it 'response with JSON body containing expected Job Signature attributes' do
      expect(hash_body.keys).to match_array([:line_products])
      expect(hash_body[:line_products].size).to eq(3)
      items = %i[id line product work_calendars]
      expect(hash_body[:line_products].first.keys).to match_array(items)
    end
  end

  describe 'GET #show' do
    it 'returns a success response' do
      sign_in
      line_product = create(:line_product)
      get :show, params: { id: line_product.id }
      expect(response).to be_successful
    end
  end
end
