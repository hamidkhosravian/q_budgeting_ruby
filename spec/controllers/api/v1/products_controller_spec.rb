# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Api::V1::ProductsController, type: :controller do
  describe 'GET #index' do
    before do
      budget_period = create(:budget_period)
      root = create(:product, budget_period: budget_period)
      create_list(:product, 2, parent: root, budget_period: budget_period)
      sign_in
      get :index, params: { budget_period_id: budget_period.id }
    end

    it 'returns http success' do
      expect(response).to have_http_status(:ok)
    end

    it 'response with JSON body containing expected Job Signature attributes' do
      expect(hash_body.keys).to match_array([:products])
      expect(hash_body[:products].size).to eq(1)
      items = %i[id code name description product_type children budget_period]
      expect(hash_body[:products].first.keys).to match_array(items)
    end
  end

  describe 'POST #Create' do
    it 'returns http created' do
      sign_in
      budget_period = create(:budget_period)
      post :create, params: attributes_for(:product).merge(budget_period_id: budget_period.id)
      expect(response).to have_http_status(:created)
    end
  end

  describe 'PUT #Update' do
    let(:product) { create(:product) }
    let(:updated) { attributes_for(:product).merge(id: product.id) }

    before do
      sign_in
      put :update, params: updated
    end

    it 'returns http success' do
      expect(response).to have_http_status(:ok)
    end
  end

  describe 'DELETE #Destroy' do
    let(:product) { create(:product) }

    before do
      sign_in
      delete :destroy, params: { id: product.id }
    end

    it 'returns http no content' do
      expect(response).to have_http_status(:no_content)
    end
  end

  describe 'GET # of products of line' do
    before do
      line = create(:line)
      create_list(:line_product, 3, line: line)
      sign_in
      get :index, params: { line_id: line.id }
    end

    it 'returns http success' do
      expect(response).to have_http_status(:ok)
    end

    it 'response with JSON body containing expected Job Signature attributes' do
      expect(hash_body.keys).to match_array([:products])
      expect(hash_body[:products].size).to eq(3)
      items = %i[id code name description product_type children budget_period]
      expect(hash_body[:products].first.keys).to match_array(items)
    end
  end
end
