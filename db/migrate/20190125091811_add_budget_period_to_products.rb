class AddBudgetPeriodToProducts < ActiveRecord::Migration[5.2]
  def change
    add_reference :products, :budget_period, foreign_key: true
  end
end
