class AddBudgetPeriodToMaterials < ActiveRecord::Migration[5.2]
  def change
    add_reference :materials, :budget_period, foreign_key: true
  end
end
