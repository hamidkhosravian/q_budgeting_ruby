class CreateLines < ActiveRecord::Migration[5.2]
  def change
    create_table :lines do |t|
      t.references :factory, foreign_key: true
      t.string :code
      t.string :name
      t.text :description

      t.timestamps
    end
  end
end
