# frozen_string_literal: true

class CreateBudgetPeriods < ActiveRecord::Migration[5.2]
  def change
    create_table :budget_periods do |t|
      t.string :code
      t.string :name
      t.datetime :start_date
      t.datetime :end_date
      t.string :description

      t.timestamps
    end
  end
end
