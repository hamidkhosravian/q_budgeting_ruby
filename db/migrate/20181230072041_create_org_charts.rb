class CreateOrgCharts < ActiveRecord::Migration[5.2]
  def change
    create_table :org_charts do |t|
      t.references :user, foreign_key: true
      t.string :position
      t.string :ancestry

      t.timestamps
    end
    add_index :org_charts, :ancestry
  end
end
