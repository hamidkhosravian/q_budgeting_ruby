class AddDetailsToLines < ActiveRecord::Migration[5.2]
  def change
    add_column :lines, :start_from, :date
    add_column :lines, :end_from, :date
    add_reference :lines, :budget_period, foreign_key: true
  end
end
