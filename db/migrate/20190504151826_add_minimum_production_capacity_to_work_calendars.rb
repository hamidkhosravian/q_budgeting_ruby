class AddMinimumProductionCapacityToWorkCalendars < ActiveRecord::Migration[5.2]
  def change
    add_column :work_calendars, :minimum_production_capacity, :integer
  end
end
