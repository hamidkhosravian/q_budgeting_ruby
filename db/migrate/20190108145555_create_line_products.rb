class CreateLineProducts < ActiveRecord::Migration[5.2]
  def change
    create_table :line_products do |t|
      t.references :line, foreign_key: true
      t.references :product, foreign_key: true

      t.timestamps
    end
  end
end
