class CreateProducts < ActiveRecord::Migration[5.2]
  def change
    create_table :products do |t|
      t.string :name
      t.string :code
      t.string :description
      t.integer :product_type
      t.string :ancestry

      t.timestamps
    end
    add_index :products, :ancestry
  end
end
