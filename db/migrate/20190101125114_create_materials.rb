class CreateMaterials < ActiveRecord::Migration[5.2]
  def change
    create_table :materials do |t|
      t.string :code
      t.string :name
      t.string :description
      t.string :ancestry

      t.timestamps
    end
  end
end
