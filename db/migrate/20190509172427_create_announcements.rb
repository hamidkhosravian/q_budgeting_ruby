class CreateAnnouncements < ActiveRecord::Migration[5.2]
  def change
    create_table :announcements do |t|
      t.references :budget_period, foreign_key: true
      t.references :sales_channel, foreign_key: true
      t.decimal :total_percentage

      t.timestamps
    end
  end
end
