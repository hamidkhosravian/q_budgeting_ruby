class CreateProfiles < ActiveRecord::Migration[5.2]
  def change
    create_table :profiles do |t|
      t.references :user, foreign_key: true
      t.string :avatar
      t.string :personal_code
      t.string :national_code
      t.string :first_name
      t.string :last_name
      t.string :city
      t.string :state
      t.string :postal_code
      t.string :address
      t.string :phone_number
      t.string :description
      t.datetime :hired_at

      t.timestamps
    end
  end
end
