class CreateFactories < ActiveRecord::Migration[5.2]
  def change
    create_table :factories do |t|
      t.string :code
      t.string :name
      t.string :city
      t.string :state
      t.string :postal_code
      t.string :phone_number
      t.text :address
      t.text :description

      t.timestamps
    end
  end
end
