class CreateWorkCalendars < ActiveRecord::Migration[5.2]
  def change
    create_table :work_calendars do |t|
      t.references :line_product, foreign_key: true
      t.datetime :month
      t.integer :days_of_month
      t.integer :legal_holidays
      t.integer :factory_holidays
      t.integer :technical_holidays
      t.integer :number_of_working_days
      t.integer :number_of_shift_hours
      t.integer :maximum_production_capacity

      t.timestamps
    end
  end
end
