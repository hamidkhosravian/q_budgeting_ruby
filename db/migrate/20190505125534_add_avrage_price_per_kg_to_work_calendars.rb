class AddAvragePricePerKgToWorkCalendars < ActiveRecord::Migration[5.2]
  def change
    add_column :work_calendars, :avrage_price_per_kg, :decimal
  end
end
