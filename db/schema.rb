# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_05_05_125534) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "auth_tokens", force: :cascade do |t|
    t.string "token"
    t.bigint "user_id"
    t.datetime "expire_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["token"], name: "index_auth_tokens_on_token", unique: true
    t.index ["user_id"], name: "index_auth_tokens_on_user_id"
  end

  create_table "budget_periods", force: :cascade do |t|
    t.string "code"
    t.string "name"
    t.datetime "start_date"
    t.datetime "end_date"
    t.string "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "factories", force: :cascade do |t|
    t.string "code"
    t.string "name"
    t.string "city"
    t.string "state"
    t.string "postal_code"
    t.string "phone_number"
    t.text "address"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "line_products", force: :cascade do |t|
    t.bigint "line_id"
    t.bigint "product_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["line_id"], name: "index_line_products_on_line_id"
    t.index ["product_id"], name: "index_line_products_on_product_id"
  end

  create_table "lines", force: :cascade do |t|
    t.bigint "factory_id"
    t.string "code"
    t.string "name"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.date "start_from"
    t.date "end_from"
    t.bigint "budget_period_id"
    t.index ["budget_period_id"], name: "index_lines_on_budget_period_id"
    t.index ["factory_id"], name: "index_lines_on_factory_id"
  end

  create_table "materials", force: :cascade do |t|
    t.string "code"
    t.string "name"
    t.string "description"
    t.string "ancestry"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "budget_period_id"
    t.index ["budget_period_id"], name: "index_materials_on_budget_period_id"
  end

  create_table "org_charts", force: :cascade do |t|
    t.bigint "user_id"
    t.string "position"
    t.string "ancestry"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["ancestry"], name: "index_org_charts_on_ancestry"
    t.index ["user_id"], name: "index_org_charts_on_user_id"
  end

  create_table "products", force: :cascade do |t|
    t.string "name"
    t.string "code"
    t.string "description"
    t.integer "product_type"
    t.string "ancestry"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "budget_period_id"
    t.index ["ancestry"], name: "index_products_on_ancestry"
    t.index ["budget_period_id"], name: "index_products_on_budget_period_id"
  end

  create_table "profiles", force: :cascade do |t|
    t.bigint "user_id"
    t.string "avatar"
    t.string "personal_code"
    t.string "national_code"
    t.string "first_name"
    t.string "last_name"
    t.string "city"
    t.string "state"
    t.string "postal_code"
    t.string "address"
    t.string "phone_number"
    t.string "description"
    t.datetime "hired_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_profiles_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", null: false
    t.string "encrypted_password", null: false
    t.integer "role", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
  end

  create_table "work_calendars", force: :cascade do |t|
    t.bigint "line_product_id"
    t.datetime "month"
    t.integer "days_of_month"
    t.integer "legal_holidays"
    t.integer "factory_holidays"
    t.integer "technical_holidays"
    t.integer "number_of_working_days"
    t.integer "number_of_shift_hours"
    t.integer "maximum_production_capacity"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "minimum_production_capacity"
    t.decimal "avrage_price_per_kg"
    t.index ["line_product_id"], name: "index_work_calendars_on_line_product_id"
  end

  add_foreign_key "auth_tokens", "users"
  add_foreign_key "line_products", "lines"
  add_foreign_key "line_products", "products"
  add_foreign_key "lines", "budget_periods"
  add_foreign_key "lines", "factories"
  add_foreign_key "materials", "budget_periods"
  add_foreign_key "org_charts", "users"
  add_foreign_key "products", "budget_periods"
  add_foreign_key "profiles", "users"
  add_foreign_key "work_calendars", "line_products"
end
